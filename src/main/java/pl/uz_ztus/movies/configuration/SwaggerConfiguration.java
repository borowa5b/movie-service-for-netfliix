package pl.uz_ztus.movies.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .tags(new Tag("Movies", "Movies operations"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("pl.uz_ztus.movies.api"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Movies service API")
                .description("API for managing movies in NetflIIx app")
                .contact(new Contact("Mateusz Borowski",
                        null,
                        "borowa5b@gmail.com"))
                .license("MIT Licence")
                .licenseUrl("https://opensource.org/licenses/mit-license.php")
                .build();
    }
}
