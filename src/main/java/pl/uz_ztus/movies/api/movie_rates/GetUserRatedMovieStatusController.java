package pl.uz_ztus.movies.api.movie_rates;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.api.MoviesController;
import pl.uz_ztus.movies.domain.model.ThumbDirections;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class GetUserRatedMovieStatusController {

    private final MovieService movieService;

    @Autowired
    public GetUserRatedMovieStatusController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/rate/get")
    @ApiOperation("Gets movie rate")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movie rate fetched"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public ThumbDirections getMovie(@RequestParam final Long movieId,
                                    @RequestParam final Long userId) {
        return this.movieService.getUserRatedMovieStatus(movieId, userId);
    }
}
