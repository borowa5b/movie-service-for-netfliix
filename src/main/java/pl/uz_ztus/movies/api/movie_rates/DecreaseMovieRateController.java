package pl.uz_ztus.movies.api.movie_rates;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.api.MoviesController;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class DecreaseMovieRateController {

    private final MovieService movieService;

    @Autowired
    public DecreaseMovieRateController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @PutMapping(value = "/rate/decrease")
    @ApiOperation("Decreases movie thumbs up/down")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movie unrated"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public int decreaseMovieRate(@RequestParam final boolean isThumbDown,
                                 @RequestParam final Long movieId,
                                 @RequestParam final Long userId) {
        return this.movieService.decreaseMovieRate(isThumbDown, movieId, userId);
    }
}
