package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class DeleteFromSavedMoviesController {

    private final MovieService movieService;

    @Autowired
    public DeleteFromSavedMoviesController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @DeleteMapping("/saved/{movieId}/{userId}")
    @ApiOperation("Deletes movie from saved")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movie deleted from saved"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public void deleteSavedMovie(@PathVariable final Long movieId,
                                 @PathVariable final Long userId) {
        this.movieService.deleteFromSavedMovies(movieId, userId);
    }
}
