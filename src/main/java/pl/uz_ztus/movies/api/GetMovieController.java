package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.domain.model.response.MovieJson;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class GetMovieController {

    private final MovieService movieService;

    @Autowired
    public GetMovieController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movie")
    @ApiOperation("Gets movie")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movie fetched"),
            @ApiResponse(code = 404, message = "Movie not found"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public MovieJson getMovie(@RequestParam final String title,
                              @RequestParam final Long userId) {
        return this.movieService.get(title, userId);
    }
}
