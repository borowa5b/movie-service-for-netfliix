package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.uz_ztus.movies.domain.model.MovieData;
import pl.uz_ztus.movies.domain.model.dao.Movie;
import pl.uz_ztus.movies.domain.service.MovieService;

import javax.validation.Valid;

@MoviesController
public class AddMovieController {

    private final MovieService movieService;

    @Autowired
    public AddMovieController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation("Adds new movie")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Movie added"),
            @ApiResponse(code = 404, message = "Category not found / invalid category"),
            @ApiResponse(code = 409, message = "Movie already exists"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public Movie addMovie(@RequestBody @Valid final MovieData movieData) {
        return this.movieService.add(movieData);
    }
}
