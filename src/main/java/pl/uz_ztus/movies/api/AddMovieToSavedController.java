package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class AddMovieToSavedController {

    private final MovieService movieService;

    @Autowired
    public AddMovieToSavedController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping(value = "/save")
    @ApiOperation("Adds movie")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Movie added to saved list"),
            @ApiResponse(code = 404, message = "Movie not found"),
            @ApiResponse(code = 409, message = "Movie is already in saved list"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public void saveMovie(@RequestParam final Long movieId, @RequestParam final Long userId) {
        this.movieService.addToSaved(movieId, userId);
    }
}
