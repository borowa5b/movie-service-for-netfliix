package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.domain.model.dao.Movie;
import pl.uz_ztus.movies.domain.service.MovieService;

import java.util.List;

@MoviesController
public class GetSavedMoviesController {

    private final MovieService movieService;

    @Autowired
    public GetSavedMoviesController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/saved")
    @ApiOperation("Gets all saved movies")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movies fetched"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public List<Movie> getSavedMovies(@RequestParam final Long userId) {
        return this.movieService.getSaved(userId);
    }
}
