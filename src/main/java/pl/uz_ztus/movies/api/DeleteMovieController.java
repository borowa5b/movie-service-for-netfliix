package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.uz_ztus.movies.domain.service.MovieService;

@MoviesController
public class DeleteMovieController {

    private final MovieService movieService;

    @Autowired
    public DeleteMovieController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @DeleteMapping("/delete/{movieId}")
    @ApiOperation("Deletes movie")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movie deleted"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public void deleteMovie(@PathVariable final Long movieId) {
        this.movieService.deleteMovie(movieId);
    }
}
