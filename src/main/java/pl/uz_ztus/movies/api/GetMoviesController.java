package pl.uz_ztus.movies.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.movies.domain.model.response.MovieJson;
import pl.uz_ztus.movies.domain.service.MovieService;

import java.util.List;

@MoviesController
public class GetMoviesController {

    private final MovieService movieService;

    @Autowired
    public GetMoviesController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping()
    @ApiOperation("Gets all movies")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Movies fetched"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public List<MovieJson> getMoviesByCategory(@RequestParam final Long userId) {
        return this.movieService.getAll(userId);
    }
}
