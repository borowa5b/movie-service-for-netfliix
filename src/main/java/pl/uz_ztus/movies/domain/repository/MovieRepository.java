package pl.uz_ztus.movies.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.uz_ztus.movies.domain.model.dao.Movie;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Movie getByTitle(final String title);

    List<Movie> getMoviesByCategory(final String categoqry);

    Movie getById(final Long movieId);

    @Query(value = "SELECT timestamp FROM movie_saved_timestamp mst " +
            "WHERE mst.user_id = ?1 AND mst.movie_id = ?2",
            nativeQuery = true)
    Long getMovieTimestamp(final Long userId, final Long movieId);

    @Modifying
    @Query(value = "INSERT INTO favourite_movies(movie_id, user_id) VALUES (?1, ?2)",
            nativeQuery = true)
    void addToSaved(final Long userId, final Long movieId);

    @Query(value = "SELECT movie_id FROM favourite_movies WHERE user_id = ?1", nativeQuery = true)
    List<BigInteger> getSaved(final Long userId);

    @Modifying
    @Query(value = "DELETE FROM movies WHERE id = ?1", nativeQuery = true)
    void deleteById(final Long movieId);

    @Modifying
    @Query(value = "DELETE FROM favourite_movies WHERE movie_id = ?1 AND user_id = ?2",
            nativeQuery = true)
    void deleteFromSaved(final Long userId, final Long movieId);

    @Modifying
    @Query(value = "UPDATE movies SET thumbs_up = ?1 WHERE id = ?2",
            nativeQuery = true)
    void setMovieThumbsUp(final int thumbsUp, final Long movieId);

    @Modifying
    @Query(value = "UPDATE movies SET thumbs_down = ?1 WHERE id = ?2",
            nativeQuery = true)
    void setMovieThumbsDown(int thumbsDown, final Long movieId);

    @Modifying
    @Query(value = "INSERT INTO rated_movies (" +
            "thumb_direction, " +
            "movie_id, " +
            "user_id) " +
            "VALUES (?1, ?2, ?3)",
            nativeQuery = true)
    void setUserRatedMovie(final String thumbDirection, final Long movieId, final Long userId);

    @Modifying
    @Query(value = "DELETE FROM rated_movies WHERE movie_id = ?1 AND user_id = ?2",
            nativeQuery = true)
    void unsetUserRatedMovie(final Long movieId, final Long userId);

    @Query(value = "SELECT thumb_direction FROM rated_movies WHERE movie_id = ?1 AND user_id = ?2",
            nativeQuery = true)
    String getUserRatedMovieStatus(final Long movieId, final Long userId);
}
