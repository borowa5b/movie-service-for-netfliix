package pl.uz_ztus.movies.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uz_ztus.movies.domain.exception.InvalidCategoryException;
import pl.uz_ztus.movies.domain.exception.MovieAlreadyExistsException;
import pl.uz_ztus.movies.domain.exception.MovieNotFoundException;
import pl.uz_ztus.movies.domain.model.Category;
import pl.uz_ztus.movies.domain.model.MovieData;
import pl.uz_ztus.movies.domain.model.ThumbDirections;
import pl.uz_ztus.movies.domain.model.dao.Movie;
import pl.uz_ztus.movies.domain.model.response.MovieJson;
import pl.uz_ztus.movies.domain.repository.MovieRepository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(final MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Transactional
    public Movie add(final MovieData movieData) {
        final Category category = movieData.getCategory();
        if (!Category.isValid(category)) {
            throw new InvalidCategoryException(category.toString());
        }
        final String title = movieData.getTitle();
        if (isInDatabase(title)) {
            throw new MovieAlreadyExistsException(title);
        }
        final Movie movie = new Movie(movieData);
        return this.movieRepository.save(movie);
    }

    @Transactional
    public void addToSaved(final Long movieId, final Long userId) {
        final Movie movie = getById(movieId);
        if (!isInDatabase(movie.getTitle())) {
            throw new MovieNotFoundException(movie.getTitle());
        }
        if (isInSavedMovies(movieId, userId)) {
            throw new MovieAlreadyExistsException(movie.getTitle());
        }
        this.movieRepository.addToSaved(movieId, userId);
    }

    public void deleteMovie(final Long movieId) {
        this.movieRepository.deleteById(movieId);
    }

    @Transactional
    public void deleteFromSavedMovies(final Long movieId, final Long userId) {
        this.movieRepository.deleteFromSaved(movieId, userId);
    }

    public MovieJson get(final String title, final Long userId) {
        final Movie movie = movieRepository.getByTitle(title);
        if (Objects.isNull(movie)) {
            throw new MovieNotFoundException(title);
        }
        final Long timestamp = movieRepository.getMovieTimestamp(userId, movie.getId());
        final boolean isInSaved = isInSavedMovies(movie.getId(), userId);
        return new MovieJson(movie.getId(),
                movie.getTitle(),
                movie.getDescription(),
                movie.getCategory(),
                movie.getThumbnailUrl(),
                movie.getMovieUrl(),
                timestamp,
                isInSaved,
                movie.getThumbsUp(),
                movie.getThumbsDown());
    }

    private Movie getById(final Long id) {
        return this.movieRepository.getById(id);
    }

    public List<Movie> getSaved(final Long userId) {
        final List<BigInteger> savedMoviesIds = this.movieRepository.getSaved(userId);
        final List<Movie> savedMovies = new ArrayList<>();
        savedMoviesIds.forEach(id -> savedMovies.add(getById(id.longValue())));
        return savedMovies;
    }

    public List<MovieJson> getByCategory(final Category category, final Long userId) {
        final String categoryString = category.toString();
        if (!Category.isValid(category)) {
            throw new InvalidCategoryException(categoryString);
        }
        final List<Movie> moviesByCategory = movieRepository.getMoviesByCategory(categoryString);
        return getMovieJsons(userId, moviesByCategory);
    }

    private List<MovieJson> getMovieJsons(final Long userId, final List<Movie> moviesByCategory) {
        final List<MovieJson> movieJsons = new ArrayList<>();
        moviesByCategory.forEach(movie -> {
            final Long timestamp = movieRepository.getMovieTimestamp(userId, movie.getId());
            final boolean isInSaved = isInSavedMovies(movie.getId(), userId);
            movieJsons.add(new MovieJson(movie.getId(),
                    movie.getTitle(),
                    movie.getDescription(),
                    movie.getCategory(),
                    movie.getThumbnailUrl(),
                    movie.getMovieUrl(),
                    timestamp,
                    isInSaved,
                    movie.getThumbsUp(),
                    movie.getThumbsDown()));
        });
        return movieJsons;
    }

    public List<MovieJson> getAll(final Long userId) {
        final List<Movie> all = movieRepository.findAll();
        return getMovieJsons(userId, all);
    }

    private boolean isInDatabase(final String title) {
        final Movie movie = movieRepository.getByTitle(title);
        return Objects.nonNull(movie);
    }

    private boolean isInSavedMovies(final Long movieId, final Long userId) {
        final List<Movie> saved = getSaved(userId);
        final Optional<Movie> optionalMovie =
                saved.stream().filter(movie -> movie.getId().equals(movieId)).findFirst();
        return optionalMovie.isPresent();
    }

    @Transactional
    public int increaseMovieRate(final boolean isThumbDown,
                                 final Long movieId,
                                 final Long userId) {
        final Movie movie = getById(movieId);
        final int newThumbsNumber;

        if (isThumbDown) {
            final int thumbsDown = movie.getThumbsDown() + 1;
            this.movieRepository.setMovieThumbsDown(
                    thumbsDown,
                    movieId);
            this.movieRepository.setUserRatedMovie(ThumbDirections.DOWN.toString(),
                    movieId,
                    userId);
            newThumbsNumber = thumbsDown;
        } else {
            final int thumbsUp = movie.getThumbsUp() + 1;
            this.movieRepository.setMovieThumbsUp(
                    thumbsUp,
                    movieId);
            this.movieRepository.setUserRatedMovie(ThumbDirections.UP.toString(),
                    movieId,
                    userId);
            newThumbsNumber = thumbsUp;
        }
        return newThumbsNumber;
    }

    @Transactional
    public int decreaseMovieRate(final boolean isThumbDown,
                                 final Long movieId,
                                 final Long userId) {
        final Movie movie = getById(movieId);
        final int newThumbsNumber;

        if (isThumbDown) {
            final int thumbsDown = movie.getThumbsDown() - 1;
            this.movieRepository.setMovieThumbsDown(
                    thumbsDown,
                    movieId);
            newThumbsNumber = thumbsDown;
        } else {
            final int thumbsUp = movie.getThumbsUp() - 1;
            this.movieRepository.setMovieThumbsUp(
                    thumbsUp,
                    movieId);
            newThumbsNumber = thumbsUp;
        }
        this.movieRepository.unsetUserRatedMovie(movieId,
                userId);
        return newThumbsNumber;
    }

    public ThumbDirections getUserRatedMovieStatus(final Long movieId, final Long userId) {
        final String userRatedMovieStatus =
                this.movieRepository.getUserRatedMovieStatus(movieId, userId);
        if (Objects.isNull(userRatedMovieStatus)) {
            return ThumbDirections.NONE;
        }

        if (userRatedMovieStatus.equals(ThumbDirections.UP.toString())) {
            return ThumbDirections.UP;
        } else {
            return ThumbDirections.DOWN;
        }
    }
}
