package pl.uz_ztus.movies.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MovieData {
    @NotNull
    @NotBlank
    private final String title;
    @NotNull
    @NotBlank
    private final String description;
    @NotNull
    private final Category category;
    @NotNull
    @NotBlank
    private final String thumbnailUrl;
    @NotNull
    @NotBlank
    private final String movieUrl;
}
