package pl.uz_ztus.movies.domain.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MovieJson {

    private Long id;
    private String title;
    private String description;
    private String category;
    private String thumbnailUrl;
    private String movieUrl;
    private Long timestamp;
    private boolean isInSaved;
    private int thumbsUp;
    private int thumbsDown;
}
