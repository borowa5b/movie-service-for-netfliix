package pl.uz_ztus.movies.domain.model;

public enum Category {

    COMEDY,
    HORROR,
    SCI_FI,
    THRILLER;

    public static boolean isValid(final Category givenCategory) {
        for (final Category category : Category.values()) {
            if (category == givenCategory) {
                return true;
            }
        }
        return false;
    }
}
