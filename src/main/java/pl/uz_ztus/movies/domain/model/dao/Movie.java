package pl.uz_ztus.movies.domain.model.dao;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.uz_ztus.movies.domain.model.MovieData;

import javax.persistence.*;

@Data
@Entity
@Table(name = "movies")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true, nullable = false)
    private String title;
    private String description;
    private String category;
    private String thumbnailUrl;
    private String movieUrl;
    private int thumbsUp;
    private int thumbsDown;

    public Movie(final MovieData movieData) {
        this.title = movieData.getTitle();
        this.description = movieData.getDescription();
        this.category = movieData.getCategory().name();
        this.thumbnailUrl = movieData.getThumbnailUrl();
        this.movieUrl = movieData.getMovieUrl();
    }
}
