package pl.uz_ztus.movies.domain.model;

public enum ThumbDirections {
    NONE,
    UP,
    DOWN
}
