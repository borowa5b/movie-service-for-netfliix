package pl.uz_ztus.movies.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidCategoryException extends RuntimeException {

    public InvalidCategoryException(final String category) {
        super(MessageFormat.format("Category {0} is not valid one", category));
    }
}
