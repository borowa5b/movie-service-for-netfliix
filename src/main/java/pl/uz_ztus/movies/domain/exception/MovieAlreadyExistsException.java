package pl.uz_ztus.movies.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

@ResponseStatus(HttpStatus.CONFLICT)
public class MovieAlreadyExistsException extends RuntimeException {

    public MovieAlreadyExistsException(final String title) {
        super(MessageFormat.format("Movie with title {0} already exists", title));
    }
}
